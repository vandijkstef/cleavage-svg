const XMLJS = require('xml-js');

export class SVG {

	_inputString: string; // The SVG as a string
	_data: any; // JSON SVG
	_svgID: string; // SVG ID
	_fileName: string; // SVG Filename 
	_status: string; // Clean status
	_parsed: any; // Parsed data
	XMLOptions: object; // Options for XMLJSON module

	constructor(SVG: string, fileName: string = '') {
		this._data = {};
		this._inputString = SVG;
		this._status = 'DIRTY';
		this._fileName = fileName;

		this.XMLOptions = {
			alwaysArray: true
		}
	}

	SetString(SVG: string) {
		this._status = 'DIRTY';
		this._inputString = SVG;
	}

	ParseSVG() {
		if (this._inputString.length == 0) {
			this._status = 'NO_DATA'
		} else {
			let jsondata:any;
			try {
				jsondata = XMLJS.xml2js(this._inputString, this.XMLOptions);
			} catch(err) {
				this._status = 'BAD_DATA'
				return;
			}
			let isSVG: boolean = false;
			jsondata.elements.forEach((element) => {
				if (element.type == 'element' && element.name == 'svg') {
					this._data = jsondata;
					// this._svgID = element.attributes.id;
					this.UpdateID(element.attributes.id);
					isSVG = true;
				}
			});
			if (!isSVG) {
				this._status = 'FALSE_DATA';
			}
		}
	}

	Clean() {

		if (this._data.declaration && true) { // TODO: Implement remove XML declaration
			delete this._data.declaration;
		}

		for(let a = this._data.elements.length - 1; a >= 0; a--) {
			if (this._data.elements[a].type == 'comment' && true) { // TODO: Implement remove comments
				delete this._data.elements[a];
				continue;
			}
			if (this._data.elements[a].type == 'element' && this._data.elements[a].elements) {
				for(let b = this._data.elements[a].elements.length - 1; b >= 0; b--) {
					if (this._data.elements[a].elements[b].name == 'g' && !this._data.elements[a].elements[b].elements && true) { // TODO: Implement empty group clearing
						delete this._data.elements[a].elements[b];
						continue;
					}
				}
			}
		}

		this._parsed = XMLJS.js2xml(this._data);
		this._status = 'CLEAN';
		
	}

	GetData(parsed: boolean = false) {

		if (this._status == 'CLEAN') {
			if (parsed) {
				return this._parsed;
			} else {
				return this._data;
			}
		} else {
			return new Error(this._status);
		}

	}

	UpdateID(id: string) {
		this._svgID = id;
	}

}
