import { Cleavage, CleavageOptions } from './Cleavage';
import { SVGHTML } from './HTML.SVG';
import { UITools } from '../public/vbase/scripts/ts/UI';

interface CleavageHTMLOptions extends CleavageOptions {
	svgWrapper: HTMLElement;
}

interface CleavageInput extends HTMLInputElement {
	Cleavage?: CleavageHTML;
}

interface CleavageFileReader extends FileReader {
	fileName?: string;
}

export class CleavageHTML extends Cleavage {

	HTMLOptions: CleavageHTMLOptions;
	UI: UITools;

	constructor(options?: CleavageHTMLOptions) {
		super(options);
		this.HTMLOptions = {
			svgWrapper: options.svgWrapper
		}

		this.UI = new UITools();
		 
	}

	ReadFiles(this: CleavageInput) {
		const files: Array<File> = Array.from(this.files);
		const Cleavage = this.Cleavage;
		files.forEach((file) => {
			const reader: CleavageFileReader = new FileReader();
			reader.onload = function(e: any) {
				const newsvg = new SVGHTML(document.querySelector('form'), e.target.result, e.target.fileName);
				Cleavage.AddIcon(newsvg);
			}
			reader.fileName = file.name
			reader.readAsText(file);
		});
	}

	GenerateFileInput() {
		// const fileWrapper: HTMLElement = this.UI.Create; // TODO: Finish UI
		// const addFiles: CleavageInput = this.UI.CreateInputText(this.UI.CreateLabel('Add SVG Files'), 'file', 'svgs'); // TODO: Finish UI

		const fileWrapper: HTMLElement = document.createElement('div');

		const addFiles: CleavageInput = document.createElement('input');
		addFiles.Cleavage = this;
		addFiles.type = 'file';
		addFiles.multiple = true;
		addFiles.accept = '.svg';
		addFiles.addEventListener('change', this.ReadFiles);
		
		const iconSets: Array<string> = ['default']; // TODO: Get from somewhere else // TODO: Create new set
		const iconSet: HTMLSelectElement = document.createElement('select');
		iconSet.name = 'iconset';
		iconSets.forEach((set) => {
			const option = document.createElement('option');
			option.value = set;
			option.innerText = set;
			iconSet.appendChild(option);
		});

		fileWrapper.appendChild(addFiles);
		fileWrapper.appendChild(iconSet);
		return fileWrapper;
	}

	AddIcon(svg: SVGHTML, iconset: string = 'default') {
		super.AddIcon(svg, iconset);
	}

}