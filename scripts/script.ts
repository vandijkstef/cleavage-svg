import { CleavageHTML } from './HTML.Cleavage';
import { UITools } from '../public/vbase/scripts/ts/UI';

window.addEventListener('DOMContentLoaded', () => {
	// new SVGHTML(document.querySelector('form'));
	const UI = new UITools();
	const text = UI.CreateText('UITools test');
	document.body.appendChild(text);
	const CVG = new CleavageHTML({
		svgWrapper: document.querySelector('form')
	});
	document.body.appendChild(CVG.GenerateFileInput());


});