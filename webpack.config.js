const path = require('path');

module.exports = {
  entry: './dist/scripts/script.js',
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },
  mode: 'production'
};