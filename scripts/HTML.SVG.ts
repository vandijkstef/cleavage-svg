import { SVG } from './SVG';

interface SVGDOM {
	wrapper: HTMLElement;
	header: HTMLElement;
	svgTitle: HTMLHeadingElement;
	svgID: HTMLElement;
	svgFilename: HTMLElement;
	inputString: SVGInput;
	inputExample: HTMLElement;
	outputString: SVGInput;
	outputExample: HTMLElement;
}

interface SVGInput extends HTMLTextAreaElement {
	SVGHTML?: SVGHTML;
	example?: HTMLElement;
}

export class SVGHTML extends SVG {

	DOM: SVGDOM;

	constructor(parent: HTMLElement, inputString?: string, fileName?: string) {

		super(inputString || '', fileName);

		this.DOM = {
			wrapper: document.createElement('section'),
			header: document.createElement('header'),
			svgTitle: document.createElement('h2'),
			svgID: document.createElement('span'),
			svgFilename: document.createElement('span'),
			inputString: document.createElement('textarea'),
			inputExample: document.createElement('div'),
			outputString: document.createElement('textarea'),
			outputExample: document.createElement('div')
		};

		this.DOM.svgTitle.innerText = this._data && this._data.title || 'Untitled'; // TODO: SVG Title interactions
		this.DOM.header.appendChild(this.DOM.svgTitle);

		this.DOM.svgID.innerText = this._svgID;
		this.DOM.svgID.classList.add('svgid');
		this.DOM.header.appendChild(this.DOM.svgID);

		if (this._fileName.length > 0) {
			this.DOM.svgFilename.innerText = this._fileName + '.svg';
		}
		this.DOM.header.appendChild(this.DOM.svgFilename);

		this.DOM.wrapper.appendChild(this.DOM.header);

		this.DOM.inputString.SVGHTML = this;
		this.DOM.inputString.example = this.DOM.inputExample;
		this.DOM.inputString.addEventListener('change', this.UpdateExample);
		this.DOM.wrapper.appendChild(this.DOM.inputString);
		
		this.DOM.wrapper.appendChild(this.DOM.inputExample);

		this.DOM.outputString.disabled = true;
		this.DOM.wrapper.appendChild(this.DOM.outputString);
		
		this.DOM.wrapper.appendChild(this.DOM.outputExample);

		if (inputString) {
			this.DOM.inputString.value = inputString;
			this.DOM.inputString.dispatchEvent(new Event('change'));
		}

		this.DOM.wrapper.classList.add('svg');
		parent.appendChild(this.DOM.wrapper);
	}

	UpdateExample(this: SVGInput) {
		this.SVGHTML.DOM.inputExample.innerHTML = this.value;
		this.SVGHTML.SetString(this.value);
		this.SVGHTML.ParseSVG();
		this.SVGHTML.Clean();
		this.SVGHTML.DOM.outputString.value = this.SVGHTML.GetData(true);
		this.SVGHTML.DOM.outputExample.innerHTML = this.SVGHTML.DOM.outputString.value;
	}

	UpdateID(id: string) {
		super.UpdateID(id);
		this.DOM.svgID.innerText = id;
	}
}