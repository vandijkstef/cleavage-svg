import { SVG } from './SVG';

export interface CleavageOptions {
	outputstyle?: string;
	sizeAdjust?: boolean;
	sizeWidth?: number;
	sizeHeigth?: number;
	colorhandling?: string;
	idhandling?: string;
	removeComments?: boolean;
	titlehandling?: string;
}

interface Iconset {
	title: string;
	icons: Array<SVG>;
}

export class Cleavage {

	options: CleavageOptions;
	
	title?: string;

	iconsets: {[title: string]: Iconset};

	constructor(options?: CleavageOptions) {
		this.options = {
			outputstyle: options.outputstyle || 'minified',
			sizeAdjust: options.sizeAdjust || false,
			sizeWidth: options.sizeWidth || 512,
			sizeHeigth: options.sizeHeigth || 512,
			colorhandling: options.colorhandling || 'none',
			idhandling: options.idhandling || 'clear',
			removeComments: options.removeComments || true,
			titlehandling: options.titlehandling || 'remove'
		}

	}

	AddIcon(svg: SVG, iconset: string = 'default') {
		if (!this.iconsets || !this.iconsets[iconset]) {
			if (!this.iconsets) {
				this.iconsets = {};
			}
			this.iconsets[iconset] = {
				title: iconset,
				icons: Array()
			};
		}
		this.iconsets[iconset].icons.push(svg);
	}

}